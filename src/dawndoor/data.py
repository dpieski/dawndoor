import json
import btree
from contextlib import contextmanager


@contextmanager
def _get_db():
    """
    Context manager to return an instance of a database
    """
    try:
        db_file = open('/data/dawndoor.db', 'r+b')
    except OSError:
        db_file = open('/data/dawndoor.db', 'w+b')
    db = btree.open(db_file)
    yield db
    db.close()
    db_file.close()


def _get_db_entry(key, default=None, as_json=True):
    """
    Get a value out of the database

    :param key: The key to look up
    :param default: The default value if the key doesn't exist
    :param as_json: The value is stored as json, load it into a dict
    :returns: The value in teh database, or None
    """
    if isinstance(key, str):
        key = key.encode('utf8')
    with _get_db() as db:
        value = db.get(key)
    if value and as_json:
        value = json.loads(value.decode('utf8'))
    elif not value:
        value = default
    return value


def _save_db_entry(key, value):
    """
    Save a value to the database

    :param key: The key to save the data under
    :param value: The value to save, must either be a dict or a str
    """
    if isinstance(key, str):
        key = key.encode('utf8')
    if isinstance(value, str):
        value = value.encode('utf8')
    elif isinstance(value, dict):
        value = json.dumps(value).encode('utf8')
    with _get_db() as db:
        db[key] = value


def get_location():
    """
    Load the location info from flash
    """
    return _get_db_entry('location')


def save_location(latitude=None, longitude=None, timezone=None):
    """
    Save the timezone to the filesystem
    """
    _save_db_entry('location', {
        'latitude': latitude,
        'longitude': longitude,
        'timezone': timezone
    })


def get_network():
    """
    Get the WiFi config. If there is none, return None.
    """
    return _get_db_entry('network')


def save_network(**kwargs):
    """
    Write the network config to file
    """
    config = get_network()
    if not config:
        config = {}
    config.update(kwargs)
    _save_db_entry('network', config)


def get_door_status():
    """
    Load the current status of the door
    """
    return _get_db_entry('door_status', default='Closed', as_json=False)


def save_door_status(door_status):
    """
    Save the current door status
    """
    _save_db_entry('door_status', door_status)


def get_door_config():
    """
    Load the door configuration
    """
    return _get_db_entry('door_config')


def save_door_config(door_config):
    """
    Save the door configuration
    """
    _save_db_entry('door_config', door_config)


def get_sunrise_sunset(date):
    """
    Get the sunrise and sunset for the day
    """
    data = _get_db_entry('sunrise_sunset')
    if data and str(date) in data:
        return data[str(date)]
    else:
        return None


def save_sunrise_sunset(sunrise, sunset):
    """
    Save the sunrise and sunset for the day
    """
    date = str(sunrise)
    _save_db_entry('sunrise_sunset', {
        date: {
            'sunrise': sunrise.serialize(),
            'sunset': sunset.serialize()
        }
    })
