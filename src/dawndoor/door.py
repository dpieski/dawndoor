import gc
import uasyncio as asyncio
from machine import Pin
from dawndoor.data import save_door_status, get_door_config

RELAY_1 = Pin('D0', mode=Pin.OUT, pull=None, value=False)
RELAY_2 = Pin('D1', mode=Pin.OUT, pull=None, value=False)


class DoorStatus(object):
    """
    Simple enumeration class for door status
    """
    Open = 'Open'
    Closed = 'Closed'

    @classmethod
    def invert(cls, status):
        """
        Invert the status
        """
        if status == cls.Open:
            return cls.Closed
        else:
            return cls.Open


def _run_door(pin, final_status):
    """
    Open or close the door, and update the status

    :param pin: The pin to use
    :param final_status: The final status of the door (Open or Closed)
    """
    door_config = get_door_config()
    gc.collect()
    pin.on()
    await asyncio.sleep(door_config['duration'] * 1000)
    pin.off()
    save_door_status(final_status)


def open_door():
    """
    Open the door
    """
    _run_door(RELAY_1, DoorStatus.Closed)
    gc.collect()


def close_door():
    """
    Close the door
    """
    _run_door(RELAY_2, DoorStatus.Open)
    gc.collect()
