import os
import machine

from dawndoor.app import main
from dawndoor.data import get_network
from dawndoor.wifi import start_ap, stop_ap

uart = machine.UART(0, 115200)
os.dupterm(uart, 1)

network_config = get_network()
if not network_config or network_config.get('can_start_ap', True):
    start_ap()
else:
    stop_ap()

try:
    main()
except Exception as e:
    print(e)
