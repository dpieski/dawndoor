var TIMEZONES = {
    "PST": "Pacific Time",
    "AMT": "Arizona Time",
    "MST": "Mountain Time",
    "CST": "Central Time",
    "EST": "Eastern Time",
    "AST": "Atlantic Time"
};
var data = {};

function getValue(object, key) {
    var defaultValue = null;
    if (arguments.length == 3) {
        defaultValue = arguments[2];
    }
    if (object && object.hasOwnProperty(key)) {
        return object[key];
    }
    else {
        return defaultValue;
    }
}

function showAlert() {
    if ($("#config-saved").is(":hidden")) {
        $("#config-saved").slideDown();
        $("#config-saved").fadeIn();
    }
}

function hideAlert() {
    if ($("#config-saved").is(":visible")) {
        $("#config-saved").slideUp();
        $("#config-saved").fadeOut();
    }
}

function updateUI() {
    // Door
    if (data.hasOwnProperty("door")) {
        $("span.door-status").text(data["door"]["status"]);
        $("span.invert-action").text(data["door"]["action"]);
    }
    // Network
    if (data.hasOwnProperty("network")) {
        var ip_address = getValue(data["network"], "ip_address", "(unknown)");
        var essid = getValue(data["network"], "essid", "");
        var password = getValue(data["network"], "password", "");
        var can_start_ap = getValue(data["network"], "can_start_ap", true);
        $("span.ip-address").text(data["network"]["ip_address"]);
        $("#essid").val(essid);
        $("#password").val(password);
        $("#can-start-ap").prop("checked", can_start_ap);
    }
    // Location
    if (data.hasOwnProperty("location")) {
        var latitude = getValue(data["location"], "latitude", "");
        var longitude = getValue(data["location"], "longitude", "");
        var timezone = getValue(data["location"], "timezone");
        if (timezone) {
            timezoneName = TIMEZONES[data["location"]["timezone"]];
        }
        else {
            timezoneName = "(not set)";
        }
        $("#latitude").val(latitude);
        $("#longitude").val(longitude);
        $("#timezone").val(timezone);
        $("span.timezone").text(timezoneName);
    }
}

function loadData() {
    $.ajax({
        url: "/location",
        dataType: "json",
        method: "GET",
        success: function (responseData) {
            data["location"] = responseData;
            console.log(responseData);
            updateUI();
        },
        complete: function (jqXHR, textStatus) {
            console.log(textStatus);
            $.ajax({
                url: "/network",
                dataType: "json",
                method: "GET",
                success: function (responseData) {
                    console.log(responseData);
                    data["network"] = responseData;
                    updateUI();
                },
                complete: function () {
                    $.ajax({
                        url: "/door",
                        dataType: "json",
                        method: "GET",
                        success: function (responseData) {
                            data["door"] = responseData;
                            updateUI();
                        }
                    });
                }
            });
        }
    });
}

function saveLocation() {
    var params = {
        timezone: $("#timezone").val(),
        latitude: $("#latitude").val(),
        longitude: $("#longitude").val()
    }
    $.post("/location", params, function (responseData) {
        data['location'] = responseData;
        updateUI();
        showAlert();
        setTimeout(hideAlert, 5000);
    });
}

function saveNetwork() {
    var params = {
        essid: $("#essid").val(),
        password: $("#password").val(),
        can_start_ap: $("#can-start-ap").is(":checked")
    }
    $.post("/network", params, function (responseData) {
        data["network"] = responseData;
        updateUI();
        showAlert();
        setTimeout(hideAlert, 5000);
    });
}

function performDoorAction() {
}

function setUpForms() {
    // The location form
    $("#save-location").on("click", saveLocation);
    $("#save-network").on("click", saveNetwork);
}

function setUpUI() {
    // Initialise BMD
    $("body").bootstrapMaterialDesign();
    // Set up the navigation clicks on menu items and buttons
    $("a[href^='#']").on("click", function (event) {
        var elemId = $(event.target).attr("href");
        ["#getting-started", "#dashboard", "#networking", "#door", "#location"].forEach(function (item) {
            if (elemId == item) {
                $("a.nav-link[href='" + item + "']").parent().addClass("active");
                $(item).show();
            }
            else {
                $("a.nav-link[href='" + item + "']").parent().removeClass("active");
                $(item).hide();
            }
            if ($("#navbar-collapse").hasClass("show")) {
                $("#navbar-collapse").collapse("hide");
            }
            event.preventDefault();
        });
    });
    // Set up password view toggling
    $("#toggle-password").on("click", function (event) {
        var passwordInput = $("input#password");
        var button = $(event.target);
        if (passwordInput.attr("type") == "password") {
            passwordInput.attr("type", "text");
            button.text("Hide password");
        }
        else {
            passwordInput.attr("type", "password");
            button.text("Show password");
        }
    });
    // Populate the timezone select
    $("#timezone > option").remove();
    var timezoneSelect = $("#timezone");
    for (var key in TIMEZONES) {
        timezoneSelect.append('<option value="' + key + '">' + TIMEZONES[key] + "</option>");
    }
    // $("#config-saved").slideUp();
}

$(document).ready(function() {
    setUpUI();
    setUpForms();
    loadData();
});
