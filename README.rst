Dawn Door - Automatic Coop Door Opener
======================================

Dawn Door is an automatic chicken coop door opener, based on the `ESP8266`_ MCU and written in `MicroPython`_


.. _ESP8266: https://www.amazon.com/gp/product/B010O1G1ES/
.. _MicroPython: https://www.micropython.org/
