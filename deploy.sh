#!/bin/bash

ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
ESP_SDK=$ROOT/esp-open-sdk
ESP_TOOL=$ESP_SDK/esptool/esptool.py
MPYTHON=$ROOT/micropython
MPY_CROSS=$MPYTHON/mpy-cross
UNX_DIR=$MPYTHON/ports/unix
ESP_DIR=$MPYTHON/ports/esp8266
MOD_DIR=$ESP_DIR/modules
MPF_SHELL=$ROOT/mpfshell/bin/mpfshell
SRC_DIR=$ROOT/src
SHORT_PORT=ttyUSB0
PORT=/dev/$SHORT_PORT

export PATH=$UNX_DIR:$ESP_SDK/xtensa-lx106-elf/bin:$PATH

# Make sure mpy-cross is built
if [ ! -f "$MPY_CROSS/mpy-cross" ]; then
    echo "====================================="
    echo "Compiling mpy-cross"
    echo "====================================="
    cd $MPYTHON
    make DEBUG=0 -C mpy-cross
fi

# Make sure the UNIX port of MicroPython is built
if [ ! -f "$UNX_DIR/micropython" ]; then
    echo "====================================="
    echo "Compiling MycroPython"
    echo "====================================="
    cd $UNX_DIR
    make DEBUG=0
fi

# Check if the required modules are installed
if [ ! -d "$MOD_DIR/uasyncio" ]; then
    echo "====================================="
    echo "Installing asyncio"
    echo "====================================="
    $UNX_DIR/micropython -m upip install -p modules micropython-uasyncio micropython-uasyncio.core micropython-uasyncio.udp micropython-uasyncio.websocket.server
fi
if [ ! -f "$MOD_DIR/logging.py" ]; then
    echo "====================================="
    echo "Installing logging"
    echo "====================================="
    $UNX_DIR/micropython -m upip install -p modules micropython-logging
fi

# Copy the source across
echo "====================================="
echo 'Copying program files...'
echo "====================================="
cp -r $SRC_DIR/dawndoor $MOD_DIR/

# Build the ESP8266 port of MicroPython
echo "====================================="
echo 'Building ESP8266 port...'
echo "====================================="
cd $ESP_DIR
make DEBUG=0

# Deploy!
echo "====================================="
echo 'Uploading to device...'
echo "====================================="
cd $ESP_DIR
$ESP_TOOL --port $PORT erase_flash
$ESP_TOOL --port $PORT write_flash --verify --flash_size=detect --flash_mode dio 0 build/firmware-combined.bin

# Set up device
echo "====================================="
echo 'Setting up device'
echo "====================================="
# read -n1 -r -p "Please press the reset button, and then press any key to continue...\n"
cd $SRC_DIR
if [ "$DEBUG" == "1" ]; then
    $MPF_SHELL --reset --open $SHORT_PORT --script $ROOT/setup-debug.txt
else
    $MPF_SHELL --reset --open $SHORT_PORT --script $ROOT/setup.txt
fi

# Reset
echo -e "\r\nimport machine; machine.reset()\r\n" > $PORT
